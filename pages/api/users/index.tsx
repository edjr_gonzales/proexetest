import { NextApiRequest, NextApiResponse } from 'next';

import { getDB as getUserDB, getId as generateUserId, User } from 'db/users';
import method from 'const/http_req_methods';

export default async function handler(req:NextApiRequest, res:NextApiResponse) {
  const reqMethod = req.method || method.GET;

  await getUserDB()
  .then( async (UserDB) => {
    switch(reqMethod){
      case method.PUT:
        const inputData = req.body;
  
        const id = await generateUserId();
        
        const user: User = {
          id,
          name: inputData.name,
          email: inputData.email,
          username: inputData.username,
          phone: inputData.phone,
          website: inputData.website,
          address: inputData.address,
          company: inputData.company
        }
  
        UserDB.db?.data?.push(user);
        await UserDB.db?.write();
  
        return res.json({
          message: 'Created', 
          target: user
        });
  
        // break;
      default:
        return res.json({
          users: UserDB.db?.data || []
        });
    }
  })
  .catch((err) => {
    return res.status(500).json({
      message: 'Error', 
      error: err
    });
  });
  
}