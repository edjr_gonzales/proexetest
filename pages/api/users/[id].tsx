import { NextApiRequest, NextApiResponse } from 'next';


import { getDB as getUserDB } from 'db/users';
import method from 'const/http_req_methods';

export default async function handler(req:NextApiRequest, res:NextApiResponse) {
  const reqMethod = req.method || method.GET;
  
  await getUserDB()
  .then(async (UserDB) => {
    const { id } = req.query;
    const user = UserDB.chain.find({ id: Number(id) }).value() || undefined;

    if(user){
      switch(reqMethod){
        case method.PUT:
        case method.PATCH:
          const inputData = req.body;
          // update a record
          for(const attr in user){
            if(attr !== 'id' && attr in inputData){
              user[attr] = inputData[attr];
            }
          }

          await UserDB.db?.write();
          return res.json({ message: 'Updated', target: user });
          // break;
        case method.DELETE:
          // delete a record
          const deleted = UserDB.chain.remove({ id: Number(id) }).value() || false;
          return res.json({ message: 'Deleted', target: deleted });
          // break;
        default:
          return res.json({ user });
      }
    } else {
      return res.status(404).json({ error: 'User with specified ID not found!', id });
    }
  })
  .catch((err) => {
    return res.status(500).json({
      message: 'Error', 
      error: err
    });
  });
  
}