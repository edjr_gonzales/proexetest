import { Fragment, useEffect, useMemo, useState } from 'react';
import Link from 'next/link';
/* import { GetServerSideProps, InferGetServerSidePropsType } from 'next'; */
import { Box, Button, Flex, Heading, HStack, Icon, IconButton, Spacer, Table, Tbody, Td, Th, Thead, Tr, useToast } from '@chakra-ui/react';
import { FaAngleDown, FaAngleUp, FaTrash } from 'react-icons/fa';

import {
  usePagination,
  useSortBy,
  useTable,
} from 'react-table';

import useAlertDialog from 'components/alert_dialog';
import { User } from 'db/users';
import ReactTablePagination from 'components/react_table_pagination';
import method from 'const/http_req_methods';
import { useRouter } from 'next/router';

interface UsersProps {
  /* users: User[]; */
}

const Users = (props: UsersProps /*  InferGetServerSidePropsType<typeof getServerSideProps>*/ ) => {
  const { AlertWindow, openAlertDialog } = useAlertDialog();
  const [ users, setUsers ] = useState<User[]>([]);
  const [isLoading, setLoading] = useState(true);

  const toast = useToast();
  const router = useRouter();

  const deleteAccount = (userId) => {
    const requestOptions = {
      method: method.DELETE,
      headers: { 'Content-Type': 'application/json' }
    };

    fetch(`/api/users/${userId}`, requestOptions)
      .then(async (res) => {
        if(res.status === 200){
          toast({
            title: 'Account Deleted',
            description: (await res.json()).message,
            status: 'success',
            duration: 5000,
            isClosable: true,
          });

          router.reload();

        } else {
          toast({
            title: 'Error Deleting Account',
            description: res.statusText,
            status: 'error',
            duration: 5000,
            isClosable: true,
          })
        }
      });
  }

  useEffect(() => {
    setLoading(true);

    const requestOptions = {
      method: method.GET,
      headers: { 'Content-Type': 'application/json' },
    };

    fetch('/api/users', requestOptions)
      .then((res) => {
        if(res.status === 200){
          return res.json();
        } else {
          toast({
            title: 'Error Loading Accounts',
            description: res.statusText,
            status: 'error',
            duration: 5000,
            isClosable: true,
          })
          return [];
        }
      })
      .then((userList) => {
        setUsers(userList.users);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  const tableColumn: any = useMemo(
    () => [
      {
        accessor: 'id',
        Header: 'User ID',
        sortType: 'number'
      },
      {
        accessor: 'name',
        Header: 'Name',
      },
      {
        accessor: 'username',
        Header: 'User Name',
      },
      {
        accessor: 'address',
        Header: 'City',
        Cell: ({ row, value }) => (value?.city || ''),
      },
      {
        accessor: 'email',
        Header: 'Email',
      },
      {
        id: 'edit-action-column',
        Header: 'Edit',
        Cell: ({ row }) =>
          row.original.readOnly ? null : (
            <Fragment>
              <Link href={`/users/${row.original.id}/edit`} passHref>
                <Button colorScheme='blue'>Edit</Button>
              </Link>
            </Fragment>
          ),
      },
      {
        id: 'del-action-column',
        accessor: 'id',
        Header: 'Delete',
        Cell: ({ row, value }) => (
          <Fragment>
            <IconButton
              onClick={(e) => {
                openAlertDialog({
                  header: 'Delete User Confirmation',
                  message: `Are you sure that you wanted to delete User ${row.original.name}?`,
                  cancelText: 'Cancel',
                  onClose: () => {
                    
                  },
                  onConfirm: () => {
                    deleteAccount(value);
                  },
                });
              }}
              variant="unstyled"
              aria-label="Delete Offer"
              icon={<Icon as={FaTrash} />}
            />
          </Fragment>
        )
        ,
      },
    ],
    []
  );

  const tableData = useMemo(() =>  users, [users]);

  const {
    getTableProps,
    getTableBodyProps,
    headers,
    headerGroups,
    page,
    prepareRow,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns: tableColumn,
      data: tableData
    },
    useSortBy,
    usePagination,
  );

  if(isLoading) return <p>Loading...</p>;

  return (
    <Fragment>
      <HStack flex={1} p={5} m={5}>
        <Heading>Users</Heading>
        <Spacer />
        <Link href={`/users/new`} passHref>
          <Button colorScheme='blue'>Add User</Button>
        </Link>
      </HStack>

      <Table
        variant="striped"
        {...getTableProps()}
      >
        <Thead bg='ghostwhite'>
          {headerGroups.map((headerGroup, hr) => (
            <Tr key={`table-header-row-${hr}`} {...headerGroup.getHeaderGroupProps({})}>
              {headerGroup.headers.map((column, hrc) => (
                <Th key={`table-header-col-${hr}-${hrc}`}
                  {...column.getHeaderProps(
                    column.getSortByToggleProps({})
                  )}
                >
                  <HStack>
                    {column.canFilter ? (
                      <Box>{column.render('Filter')}</Box>
                    ) : null}
                    <Box>{column.render('Header')} </Box>
                    <Box>
                      {column.isSorted ? (
                        column.isSortedDesc ? (
                          <FaAngleDown />
                        ) : (
                          <FaAngleUp />
                        )
                      ) : (
                        ''
                      )}
                    </Box>
                  </HStack>
                </Th>
              ))}
            </Tr>
          ))}
        </Thead>
        <Tbody {...getTableBodyProps()}>
          {page.map((row, i) => {
            prepareRow(row);
            return (
              <Tr key={`data-row-${i}`}
                {...row.getRowProps()}
                _hover={{
                  backgroundColor: 'gray.200',
                  cursor: 'pointer',
                }}
                p={0}
              >
                {row.cells.map((cell, cellIndex) => {
                  return (
                    <Td key={`data-col-${i}-${cellIndex}`}
                      {...cell.getCellProps({})}
                      data-label={headers[cellIndex].Header}
                      title={`"${cell.value}"`}
                    >
                      {cell.render('Cell')}
                      &nbsp;
                    </Td>
                  );
                })}
              </Tr>
            );
          })}
        </Tbody>
      </Table>

      <ReactTablePagination
        canPreviousPage={canPreviousPage}
        canNextPage={canNextPage}
        pageOptions={pageOptions}
        pageCount={pageCount}
        gotoPage={gotoPage}
        nextPage={nextPage}
        previousPage={previousPage}
        setPageSize={setPageSize}
        pageIndex={pageIndex}
        pageSize={pageSize}
      />

      <AlertWindow />
    </Fragment>
  );
}

/* export const getServerSideProps: GetServerSideProps = async (context) => {
  const UserDB = await getUserDB();
  
  const users: User[] = await UserDB.db.data || [];

  return {
    props: {
      users,
    },
  };
}; */

export default Users;