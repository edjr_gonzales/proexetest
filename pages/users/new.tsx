/* import { GetServerSideProps, InferGetServerSidePropsType } from 'next'; */

import { createBlankUser, User } from 'db/users';
import UserForm from 'components/user_form';
import { useEffect, useState } from 'react';

interface NewUserProps {}

const NewUser = ( props: NewUserProps /* { user }: InferGetServerSidePropsType<typeof getServerSideProps> */) => {
  const [ userData, setUserData ] = useState<User|null>(null);

  useEffect(() => {
    setUserData({
      id: 0,
      name: '',
      email: '',
      username: '',
      phone: '',
      website: '',
      address: {
        city: ''
      },
    });
  }, []);

  return (
    userData && <UserForm user={userData} />
  );
}

/* export const getServerSideProps: GetServerSideProps = async (context) => {
  const user: User = createBlankUser();

  return {
    props: {
      user,
    },
  };
}; */

export default NewUser;