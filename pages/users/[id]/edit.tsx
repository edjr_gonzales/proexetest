import { useEffect, useState } from 'react';
import { GetServerSideProps, InferGetServerSidePropsType } from 'next';
import { useRouter } from 'next/router';

import { User } from 'db/users';
import UserForm from 'components/user_form';
import method from 'const/http_req_methods';

import { useToast } from '@chakra-ui/react';

const EditUser = ({ user }: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const [isLoading, setLoading] = useState(true);
  const [ userData, setUserData ] = useState<User|null>(null);
  const toast = useToast();
  const router = useRouter();

  useEffect(() => {
    if(!user) return;

    setLoading(true);

    const requestOptions = {
      method: method.GET,
      headers: { 'Content-Type': 'application/json' },
    };

    fetch(`/api/users/${user}`, requestOptions)
      .then((res) => {
        if(res.status === 200){
          return res.json();
        } else {
          toast({
            title: 'Error Loading Account',
            description: res.statusText,
            status: 'error',
            duration: 5000,
            isClosable: true,
          });
          router.push('/users');
        }
      })
      .then((respData) => {
        setUserData(respData.user);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [user]);
  
  if(isLoading) return <p>Loading Account...</p>;

  return (
    userData && <UserForm user={userData} />
  );
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { id } = context.query;
  
  return {
    props: {
      user: id,
    },
  };
};

export default EditUser;