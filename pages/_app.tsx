import { AppProps } from 'next/app';
import { ChakraProvider, Container, Flex, Spinner } from '@chakra-ui/react';

import '../styles/globals.css';
import Layout from 'components/layout';


function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ChakraProvider>
      <Layout><Component {...pageProps} /></Layout>
    </ChakraProvider>
  )
}

export default MyApp
