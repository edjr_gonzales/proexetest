import React, { useRef, useState } from 'react';

import {
  Button,
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
} from '@chakra-ui/react';

export interface BaseAlertDialogProps {
  onClose: () => void;
  onConfirm: () => void;
  header?: JSX.Element | string;
  message: JSX.Element | string;
  cancelText?: string;
  confirmText?: string;
}

export const useAlertDialog = (): {
  AlertWindow: () => JSX.Element;
  closeAlertDialog: () => void;
  openAlertDialog: (opts: BaseAlertDialogProps) => void;
} => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [dialogState, setDialogState] = useState<BaseAlertDialogProps>({
    onClose: () => {},
    onConfirm: () => {},
    message: '',
  });

  const openAlertDialog = (props: BaseAlertDialogProps) => {
    setDialogState(props);
    setIsOpen(true);
  };

  const closeAlertDialog = () => {
    setIsOpen(false);
  };

  const AlertWindow = () => {
    const cancelRef = useRef() as React.MutableRefObject<HTMLButtonElement>;

    return (
      <AlertDialog
        isOpen={isOpen}
        leastDestructiveRef={cancelRef}
        onClose={dialogState.onClose}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              {dialogState.header || 'Please Confirm'}
            </AlertDialogHeader>

            <AlertDialogBody>{dialogState.message}</AlertDialogBody>

            <AlertDialogFooter>
              {dialogState.cancelText ? (
                <Button
                  colorScheme="red"
                  ref={cancelRef}
                  onClick={() => {
                    dialogState.onClose();
                    closeAlertDialog();
                  }}
                >
                  {dialogState.cancelText || 'Cancel'}
                </Button>
              ) : null}
              <Button
                colorScheme="blue"
                onClick={() => {
                  dialogState.onConfirm();
                  closeAlertDialog();
                }}
                ml={3}
              >
                {dialogState.confirmText || 'Ok'}
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    );
  };

  return { AlertWindow, closeAlertDialog, openAlertDialog };
};

export default useAlertDialog;
