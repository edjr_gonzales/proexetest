import React from 'react';
import {
  IconButton,
  Box,
  HStack,
  Spacer,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
  Select,
  Text,
  Wrap,
  WrapItem,
} from '@chakra-ui/react';
import {
  FaAngleLeft,
  FaAngleDoubleLeft,
  FaAngleRight,
  FaAngleDoubleRight,
} from 'react-icons/fa';

interface PaginationProps {
  pageIndex: number;
  pageSize: number;
  canPreviousPage: boolean;
  canNextPage: boolean;
  pageOptions: number[];
  pageCount: number;
  gotoPage: (updater: ((pageIndex: number) => number) | number) => void;
  nextPage: () => void;
  previousPage: () => void;
  setPageSize: (pageSize: number) => void;
}

const ReactTablePagination = (props: PaginationProps) => {
  const {
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    pageIndex,
    pageSize,
  } = props;

  if (typeof window === 'undefined') {
    return null;
  }

  return (
    <Wrap p="5">
      <Spacer />
      <WrapItem>
        <HStack>
          <IconButton
            onClick={() => gotoPage(0)}
            disabled={!canPreviousPage}
            variant="outline"
            aria-label="Goto First Page"
            icon={<FaAngleDoubleLeft />}
          />

          <IconButton
            onClick={() => previousPage()}
            disabled={!canPreviousPage}
            variant="outline"
            aria-label="Previous Page"
            icon={<FaAngleLeft />}
          />

          <Box>
            <Text>
              Page{' '}
              <strong>
                {pageIndex + 1} of {pageOptions.length}
              </strong>{' '}
            </Text>
          </Box>

          <IconButton
            onClick={() => nextPage()}
            disabled={!canNextPage}
            variant="outline"
            aria-label="Next Page"
            icon={<FaAngleRight />}
          />
          <IconButton
            onClick={() => gotoPage(pageCount - 1)}
            disabled={!canNextPage}
            variant="outline"
            aria-label="Goto Last Page"
            icon={<FaAngleDoubleRight />}
          />
        </HStack>
      </WrapItem>

      <WrapItem>
        <HStack>
          <Box>
            <Text>Go To Page:</Text>
          </Box>
          <Box>
            <NumberInput
              variant="outline"
              _invalid={{
                borderColor: 'inherit',
                boxShadow: 'inherit',
              }}
              min={0}
              max={pageCount - 1}
              value={pageIndex + 1}
              onChange={(e) => {
                const page = e ? Number(e) - 1 : 0;
                gotoPage(page);
              }}
            >
              <NumberInputField
                value={pageIndex + 1}
                _invalid={{
                  borderColor: 'inherit',
                  boxShadow: 'inherit',
                }}
              />
              <NumberInputStepper>
                <NumberIncrementStepper />
                <NumberDecrementStepper />
              </NumberInputStepper>
            </NumberInput>
          </Box>
        </HStack>
      </WrapItem>

      <WrapItem>
        <HStack>
          <Box>
            <Text>Items Per Page:</Text>
          </Box>
          <Box>
            <Select
              variant="outline"
              value={pageSize}
              onChange={(e) => {
                setPageSize(Number(e.target.value));
              }}
            >
              {[10, 20, 30, 40, 50].map((pageSize) => (
                <option key={pageSize} value={pageSize}>
                  Show {pageSize}
                </option>
              ))}
            </Select>
          </Box>
        </HStack>
      </WrapItem>
    </Wrap>
  );
};

export default ReactTablePagination;
