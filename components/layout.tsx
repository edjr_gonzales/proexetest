import React, { useEffect } from 'react';
import { Box, Flex, Spacer } from '@chakra-ui/react';

export const Layout = ({ children }) => {
  
  return (
    <Flex>
      <Spacer />
      <Box width={[
        '100%',
        '100%',
        '100%',
        '90%',
      ]} p={10} overflowX="auto">
        {children}
      </Box>
      <Spacer />
    </Flex>
    
  );
};

export default Layout;