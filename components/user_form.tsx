import { Fragment, useEffect } from 'react';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";

import { User } from 'db/users';
import { Box, Button, Flex, FormControl, FormHelperText, FormLabel, Heading, HStack, Input, Spacer, Stack, Text, useToast } from '@chakra-ui/react';

import method from 'const/http_req_methods';

import styles from './user_form.module.css';

interface UserFormProps {
  user: User;
}

const userValidationSchema = yup.object({
  name: yup.string().required('Name is required.').min(2, 'Name is too short.'),
  username: yup.string().required('Username is required.').min(2, 'Username is too short.'),
  email: yup.string().required().matches(
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    'Invalid Email Address'),
  website: yup.string().optional(),
  phone: yup.string().optional(),
  address: yup.object().shape({
    street: yup.string().optional(),
    suite: yup.string().optional(),
    city: yup.string().required('City Address is required.'),
    zipcode: yup.string().optional(),
    geo: yup.object().optional().shape({
      lat: yup.number().optional(),
      lng: yup.number().optional(),
    })
  }),
  company: yup.object().optional().shape({
    name: yup.string().optional(),
    catchPhrase: yup.string().optional(),
    bs: yup.string().optional(),
  })
}).required();

const UserForm = ({ user }: UserFormProps) => {
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<User>({ resolver: yupResolver(userValidationSchema) });

  const toast = useToast();
  const router = useRouter();

  const submitUser = (data:User) => {
    // console.log('data', data);

    const update = !!data.id && Number(data.id) !== 0;

    const requestOptions = {
      method: update ? method.PATCH : method.PUT,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(data)
    };

    fetch(`/api/users${update ? `/${data.id}` : ''}`, requestOptions)
      .then(async (res) => {
        if(res.status === 200){
          toast({
            title: 'Account Saved',
            description: (await res.json()).message,
            status: 'success',
            duration: 5000,
            isClosable: true,
          });
          router.push('/users');
        } else {
          toast({
            title: 'Error Saving Account',
            description: res.statusText,
            status: 'error',
            duration: 5000,
            isClosable: true,
          })
        }
      });
  }

  useEffect(() => {
    console.log('form errors', errors);
  }, [errors]);

  return (
    <Fragment>
      <form onSubmit={handleSubmit<User>(submitUser)}>
        <Input
          {...register('id')}
          hidden
          defaultValue={user.id}
          name="id"
          isReadOnly={true}
          />
        
        <Heading as='h5'>
          User Details
        </Heading>

        <HStack p={[0, 1, 2, 3, 4, 5]} />

        <Text as='h6'>
          Primary
        </Text>

        <Flex>
          <Box p={6}>
            <FormControl>
              <FormLabel htmlFor='name'>Name</FormLabel>
              <Input 
                {...register('name')}
                defaultValue={user.name}
                id='name' 
                name='name' 
                type='text' />
              { errors['name'] && <FormHelperText className={styles['error-text']}>{errors['name'].message}</FormHelperText> }
            </FormControl>
          </Box>
          <Box p={6}>
            <FormControl>
              <FormLabel htmlFor='username'>User Name</FormLabel>
              <Input 
                {...register('username')}
                defaultValue={user.username}
                id='username' 
                name='username' 
                type='text' />
              { errors['username'] && <FormHelperText className={styles['error-text']}>{errors['username'].message}</FormHelperText> }
            </FormControl>
          </Box>
          <Box p={6}>
            <FormControl>
              <FormLabel htmlFor='email'>Email address</FormLabel>
              <Input 
                {...register('email')}
                defaultValue={user.email}
                id='email' 
                name='email' 
                type='email' />
              { errors['email'] && <FormHelperText className={styles['error-text']}>{errors['email'].message}</FormHelperText> }
            </FormControl>
          </Box>
          <Box p={6}>
            <FormControl>
              <FormLabel htmlFor='phone'>Phone</FormLabel>
              <Input 
                {...register('phone')}
                defaultValue={user.phone}
                id='phone' 
                name='phone' 
                type='text' />
              { errors['phone'] && <FormHelperText className={styles['error-text']}>{errors['phone'].message}</FormHelperText> }
            </FormControl>
          </Box>
          <Box p={6}>
            <FormControl>
              <FormLabel htmlFor='website'>Website</FormLabel>
              <Input 
                {...register('website')}
                defaultValue={user.website}
                id='website' 
                name='website' 
                type='text' />
              { errors['website'] && <FormHelperText className={styles['error-text']}>{errors['website'].message}</FormHelperText> }
            </FormControl>
          </Box>
        </Flex>
        
        <HStack p={[0, 1, 2, 3, 4, 5]} />

        <Text as='h6'>
          Address
        </Text>

        <Flex>
          <Box p={6}>
            <FormControl>
              <FormLabel htmlFor='address-street'>Street</FormLabel>
              <Input 
                {...register('address.street')}
                defaultValue={user.address?.street}
                id='address-street' 
                name='address.street' 
                type='text' />
              { errors.address?.street && <FormHelperText className={styles['error-text']}>{errors.address?.street.message}</FormHelperText> }
            </FormControl>
          </Box>
          <Box p={6}>
          <FormControl>
              <FormLabel htmlFor='address-suite'>Suite</FormLabel>
              <Input 
                {...register('address.suite')}
                defaultValue={user.address?.suite}
                id='address-suite' 
                name='address.suite' 
                type='text' />
              { errors.address?.suite && <FormHelperText className={styles['error-text']}>{errors.address?.suite.message}</FormHelperText> }
            </FormControl>
          </Box>
          <Box p={6}>
          <FormControl>
            <FormLabel htmlFor='address-city'>City</FormLabel>
              <Input 
                {...register('address.city')}
                defaultValue={user.address?.city}
                id='address-city' 
                name='address.city' 
                type='text' />
              { errors.address?.city && <FormHelperText className={styles['error-text']}>{errors.address?.city.message}</FormHelperText> }
            </FormControl>
          </Box>
          <Box p={6}>
            <FormControl>
              <FormLabel htmlFor='address-zipcode'>Zip Code</FormLabel>
              <Input 
                {...register('address.zipcode')}
                defaultValue={user.address?.zipcode}
                id='address-zipcode' 
                name='address.zipcode' 
                type='text' />
              { errors.address?.zipcode && <FormHelperText className={styles['error-text']}>{errors.address?.zipcode.message}</FormHelperText> }
            </FormControl>
          </Box>
        </Flex>

        <Flex>
          <Box p={6}>
            <FormControl>
              <FormLabel htmlFor='address-geo-lat'>Geo Latitude</FormLabel>
              <Input 
                {...register('address.geo.lat')}
                defaultValue={user.address.geo?.lat || 0}
                id='geo-lat' 
                name='address.geo.lat' 
                type='text' />
              { errors.address?.geo?.lat && <FormHelperText className={styles['error-text']}>{errors.address?.geo?.lat.message}</FormHelperText> }
            </FormControl>
          </Box>
          <Box p={6}>
            <FormControl>
              <FormLabel htmlFor='address-geo-lng'>Geo Longitude</FormLabel>
              <Input 
                {...register('address.geo.lng')}
                defaultValue={user.address.geo?.lng || 0}
                id='geo-lng' 
                name='address.geo.lng' 
                type='text' />
              { errors.address?.geo?.lng && <FormHelperText className={styles['error-text']}>{errors.address?.geo?.lng.message}</FormHelperText> }
            </FormControl>
          </Box>
        </Flex>

        <HStack p={[0, 1, 2, 3, 4, 5]} />

        <Text as='h6'>
          Company
        </Text>

        <Flex>
          <Box p={6}>
            <FormControl>
              <FormLabel htmlFor='company-name'>Name</FormLabel>
              <Input 
                {...register('company.name')}
                defaultValue={user.company?.name}
                id='company-name' 
                name='company.name' 
                type='text' />
            </FormControl>
          </Box>
          <Box p={6}>
            <FormControl>
              <FormLabel htmlFor='company-catchPhrase'>Catch Phrase</FormLabel>
              <Input 
                {...register('company.catchPhrase')}
                defaultValue={user.company?.catchPhrase}
                id='company-catchPhrase' 
                name='company.catchPhrase' 
                type='text' />
            </FormControl>
          </Box>
          <Box p={6}>
            <FormControl>
              <FormLabel htmlFor='company-bs'>BS</FormLabel>
              <Input 
                {...register('company.bs')}
                defaultValue={user.company?.bs}
                id='company-bs' 
                name='company.bs' 
                type='text' />
            </FormControl>
          </Box>
        </Flex>

        <HStack p={[0, 1, 2, 3, 4, 5]} />
        <Stack spacing={4} direction='row' align='center'>
          <Button type='submit' colorScheme='blue'>Submit</Button>
          <Button colorScheme='gray' onClick={(e) => {
            e.preventDefault();
            e.stopPropagation();

            reset(user);
          }}>Reset
          </Button>
          <Button colorScheme='red' onClick={(e) => {
            e.preventDefault();
            e.stopPropagation();
            router.push('/users');
          }}>Cancel
          </Button>
        </Stack>

      </form>
    </Fragment>
  );
}

export default UserForm;