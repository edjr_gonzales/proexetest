import HTTP from 'http';

const methods:any = HTTP.METHODS.reduce((obj, item) => {
  return {
    ...obj,
    [item]: item,
  };
}, {});

export default methods;