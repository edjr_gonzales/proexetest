import { join, dirname, resolve } from 'path';
import { existsSync, writeFileSync, mkdirSync } from 'fs';
import { tmpdir } from 'os';
import { Low, JSONFile } from 'lowdb';
import { fileURLToPath } from 'url';
import lodash, { CollectionChain } from 'lodash';

import startingData from "./users.json";

export interface User {
  id: number;
  name: string;
  username: string;
  email: string;
  address: {
    street?: string;
    suite?: string;
    city: string;
    zipcode?: string;
    geo?: {
      lat:number;
      lng:number;
    }
  },
  phone?: string;
  website?: string;
  company?: {
    name: string;
    catchPhrase?: string;
    bs?: string;
  }
}

const userTemplate:User = {
  id: 0,
  name: '',
  email: '',
  username: '',
  phone: '',
  website: '',
  address: {
    city: ''
  },
};

export const createBlankUser = () => JSON.parse(JSON.stringify(userTemplate));

// const __dirname = dirname( fileURLToPath(import.meta.url) );

let db:Low<User[]> | null = null;
let chain: CollectionChain<User> | null = null;

export const getDB = async () => {
  if(!chain){
    const fileDev = join('.', 'db', 'users.json');
    const filePrd = join('..', 'db', 'users.json');

    if(!existsSync(fileDev) && !existsSync(filePrd)){
      const dbDir = join( tmpdir(), 'db' );
      const tmpDb = join( dbDir, 'users.json' );

      if(!existsSync( dbDir )){
        mkdirSync(dbDir);
      }

      if(!existsSync( tmpDb )){
        writeFileSync(tmpDb, JSON.stringify(startingData));
      }

      var adapter = new JSONFile<User[]>(tmpDb);
    } else {
      var adapter = new JSONFile<User[]>(existsSync(fileDev) ? fileDev : filePrd);
    }

    db = new Low(adapter);

    await db.read();
    chain = lodash.chain(db.data);
  }
  
  return { db, chain };
}

export const getId = async () => {
  const { chain } = await getDB();

  let i = 1;
   while(!!chain.find({ id: i }).value()){
    i++;
  }

  return i;
}
